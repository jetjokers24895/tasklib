package examples

import (
    _manager "taskLib/manager"
    "taskLib/job"
	"taskLib/scheduler"
)

var (
    TaskA = "A"
    TaskB = "B"
    TaskC = "C"
    TaskD = "D"
)

//RegisterTaskA "task a run every 5 seconds"
func RegisterTaskA(manager *_manager.Manager) error {
    const taskName = "A"
    manager.Add(taskName, job.JobA, scheduler.NewEvery(5, 0, 0))// task is pending
    return nil
}

//RegisterTaskA "task b run at 18:51:00 daily (utc time)"
func RegisterTaskB(manager *_manager.Manager) error {
    const taskName = "B"
    daily, err := scheduler.NewDaily(0, 3, 12)
    if err != nil {
        return err
    }
    manager.Add(taskName, job.JobB, daily)// task is pending
    return nil
}

func RegisterTaskC(manager *_manager.Manager) error {
    const taskName = "C"
    manager.Add(taskName, job.JobC, scheduler.NewEvery(5, 0, 0))// task is pending
    return nil
}

func RegisterTaskD(manager *_manager.Manager) error {
    const taskName = "D"
    manager.Add(taskName, job.JobD, scheduler.NewEvery(6, 0, 0))// task is pending
    return nil
}