package worker

import (
	"taskLib/manager"
    "taskLib/pool"
)

var Exit = make(chan bool, 1)
func Running(manager *manager.Manager) {
    println("Worker is running")
    go func() {
        for {
            select {
                case <- Exit:
                    return
                case taskName:=  <- pool.DefaultPool.Jobs:
                    task, ok := manager.Storage[taskName]
                    if ok {
                        task.DoJob()
                    } else {
                        pool.DefaultPool.Done()
                    }
            }
        }
    }()
}