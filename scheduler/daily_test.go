package scheduler

import (
	_errors "taskLib/errors"
	"testing"
	"time"
)

func TestDaily(t *testing.T) {
    t.Run("Daily is implemented by period interface", func(t *testing.T) {
        var _ Period
        var _ error
        _ , _ = NewDaily(3,0,0)
    })

    t.Run("test Daily with >60 second", func(t *testing.T) {
        _, err := NewDaily(61,0,0)
        if err == nil {
            t.Error("input second should be  <= 60")
        }

        if err != _errors.ErrSecondNotValid {
            t.Error("error was return should be", _errors.ErrSecondNotValid)
        }
    })

    t.Run("test Daily with >60 minute", func(t *testing.T) {
        _, err := NewDaily(30,61,0)
        if err == nil {
            t.Error("input minute should be  <= 60")
        }

        if err != _errors.ErrMinuteNotValid {
            t.Error("error was return should be", _errors.ErrMinuteNotValid)
        }
    })

    t.Run("test Daily with >24 hour", func(t *testing.T) {
        _, err := NewDaily(30, 50, 25)
        if err == nil {
            t.Error("input hour should be  <= 24")
        }

        if err != _errors.ErrHourNotValid {
            t.Error("error was return should be", _errors.ErrHourNotValid)
        }
    })

    t.Run("test make new Daily with valid input", func(t *testing.T) {
        _, err := NewDaily(30, 50, 21)
        if err != nil {
            t.Error("NewDaily not working right with valid input")
        }
    })

    t.Run("test NextRun", func(t *testing.T) {
        now := time.Now().UTC();
        daily, _ := NewDaily(0, 0, 17)
        nextTime := daily.NextRun()
        if nextTime.Before(now)  {
            t.Error("NextRun not working right, it should be after now")
        }
    })
}