package scheduler

import (
	"testing"
	"time"
)

func TestEvery(t *testing.T) {
    t.Run("Every is implemented by period interface", func(t *testing.T) {
        var _ Period
        var _ error
        _ = NewEvery(3,0,0)
    })

    t.Run("test next Run", func(t *testing.T) {
        every:= NewEvery(3, 0, 0)
        nextTime := every.NextRun()
        now := time.Now().UTC();
        if nextTime.Unix() - now.Unix() != 3  {
            t.Error("NextRun not working right")
        }
    })
}