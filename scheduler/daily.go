package scheduler

import(
    "time"
    "taskLib/errors"
)

type Daily struct {
    seconds int
    minutes int
    hours int
}

func (daily Daily) NextRun() time.Time {
    now := time.Now();
    year, month, day := now.Date();
    shouldAt := time.Date(year, month, day, daily.hours, daily.minutes, daily.seconds, 0, time.UTC)
    if shouldAt.After(now) {
        return shouldAt
    }

    return shouldAt.Add(24*time.Hour) // next Day
}

func (daily Daily) IsAfter() bool {
    now := time.Now();
    year, month, day := now.Date();
    shouldAt := time.Date(year, month, day, daily.hours, daily.minutes, daily.seconds, 0, time.UTC)
    return shouldAt.After(now)
}

func (daily Daily) ValidateSecond(seconds int) error {
    if seconds >= 0 && seconds <= 60 {
        return nil
    }
    return errors.ErrSecondNotValid
}

func (daily Daily) ValidateMinute(minutes int) error {
    if minutes >= 0 && minutes <= 60 {
        return nil
    }
    return errors.ErrMinuteNotValid
}

func (daily Daily) ValidateHour(hours int) error {
    if hours >= 0 && hours <= 24 {
        return nil
    }
    return errors.ErrHourNotValid
}

func (daily Daily) ValidateInput(seconds, minutes, hours int) error {
    if err := daily.ValidateSecond(seconds); err != nil {
        return err
    }

    if err := daily.ValidateMinute(minutes); err != nil {
        return err
    }

    if err := daily.ValidateHour(hours); err != nil {
        return err
    }

    return nil
}

func NewDaily(iseconds, iminutes, ihours int) (*Daily, error) {
    if err := (Daily{}).ValidateInput(iseconds, iminutes, ihours); err != nil {
        return nil, err
    }

    return &Daily{
        seconds:iseconds,
        minutes: iminutes,
        hours: ihours,
    }, nil
}
