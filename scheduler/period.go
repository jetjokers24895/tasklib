package scheduler

import (
    "time"
)

type Period interface {
    NextRun() time.Time
}