package scheduler

import("time")

type Every struct {
    seconds time.Duration
    minutes time.Duration
    hours time.Duration
}

func (e Every) NextRun() time.Time {
    duration :=  e.seconds + e.minutes +  e.hours;
    return time.Now().Add(duration)
}

func NewEvery(seconds, minutes, hours time.Duration) Every {
    return Every {
        seconds: seconds * time.Second,
        minutes: minutes * time.Minute,
        hours: hours * time.Hour,
    }
}