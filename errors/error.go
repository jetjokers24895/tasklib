package errors

import (
    "errors"
)

var  (
    ErrSecondNotValid = errors.New("input_second_is_not_valid")
    ErrMinuteNotValid = errors.New("input_minute_is_not_valid")
    ErrHourNotValid = errors.New("input_hour_is_not_valid")
    ErrTaskWasExists = errors.New("input_task_was_exists")
    ErrJobHandleMustBeFunction = errors.New("job_handle_must_be_function")
    ErrParamsNotValid = errors.New("error_params_not_valid")
)