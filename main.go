package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"strings"
	"taskLib/examples"
	_manager "taskLib/manager"
	"taskLib/worker"
	"time"
	// "taskLib/job"
	// "taskLib/scheduler"
)

// khoi tao 1 cai ticker moi giay 1 lan
// luot qua tat ca cac task
// task nao dang running thi se kiem tra shouldBeRun cua no
// neu hop le, se add vao pool

// khoi chay 1 worker, subcribe job pool
// if pause will be pass not return Done
// if stop will be return Done  for pool remove that job

func eachTick(manager *_manager.Manager) {
    fmt.Println("1 - run tick")
    for taskName, task  := range manager.Storage {
        if task.State != _manager.Running {
            continue
        }
        if task.ShouldBeDoJob() == false {
            continue
        }
        fmt.Println("1.1 - run push job", taskName)
        task.PushToPool()
    }
}

func main() {
    manager := &_manager.Manager{
        Storage: map[string]*_manager.Task{},
    } // init task manager

    worker.Running(manager) // runing one worker

    t := time.NewTicker(1*time.Second)
    exitTimeTicker := make(chan bool, 1)
    go func() {
        defer t.Stop()
        for {
            select {
            case <- t.C:
                eachTick(manager)
            case <- exitTimeTicker:
                fmt.Println("Exit time ticker, no more job will be pushed to pool!")
                return
            }
        }
    }()

    //examples
    examples.RegisterTaskA(manager)
    if err := examples.RegisterTaskB(manager); err != nil {
        log.Fatal(err)
    }
    examples.RegisterTaskC(manager)
    examples.RegisterTaskD(manager)
    manager.Start(examples.TaskA)
    manager.Start(examples.TaskB)
    manager.Start(examples.TaskC)
    manager.Start(examples.TaskD)

    c := make(chan os.Signal, 1)
    signal.Notify(c, os.Interrupt, os.Kill)
    <-c

    fmt.Println(strings.Repeat("*", 120))
    fmt.Println("Received Exit Signal")
    fmt.Println("On Graceful ShutDown:")

    // close time ticker => no job was pushed to pool
    exitTimeTicker <- true
    t2 := time.NewTicker(5 * time.Second)
    defer t2.Stop()
    // Nếu trong pool vẫn còn job thì sẽ không thể tắt được
    // Sau mỗi 5 giây sẽ kiểm tra trở lại, cho đến khi pool không còn job sẽ thoát
    for _time := range t2.C {
        fmt.Printf("*Check at: %s -  Pool have %d jobs \n", _time, manager.JobInPool())
        if manager.IsSafelyExit() {
            fmt.Println("*No job in pool")
            break
        }
    }
    //exit worker
    worker.Exit <- true
    fmt.Println("*Worker was Exited")
    fmt.Println("Exit Done! See You Again!")
    fmt.Println(strings.Repeat("*", 120))
}
