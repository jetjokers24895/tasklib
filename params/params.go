package params

var Params = map[string]interface{}{}

func Set(key string, value interface{}) {
    Params[key] = value
}

func Clear(key string) {
    Params[key] = nil
}

func Get(key string) interface{} {
    return Params[key]
}