package pool

import (
	// "fmt"
	"testing"
)

func TestPool(t *testing.T) {
    taskName := "testingTask"
    t.Run("insert a job to pool", func(t *testing.T) {
        DefaultPool.Insert(taskName)
        if DefaultPool.Size != 1 {
            t.Error("Insert a job to pool not working right, After inserting, pool size should be 1")
        }
        DefaultPool.Done()
    })

    t.Run("done a job after insert a job", func(t *testing.T) {
        DefaultPool.Insert(taskName)
        DefaultPool.Done()
        if DefaultPool.Size != 0 {
            t.Error("After Done a job, pool size should be 0")
        }
    })

    t.Run("done a job with no job in pool", func(t *testing.T) {
        DefaultPool.Done()
        if DefaultPool.Size != 0 {
            t.Error("When Done a job with no job in pool, pool size should be 0")
        }
    })

    t.Run("turnoff pool when pool have some job", func(t *testing.T) {
        DefaultPool.Insert(taskName)
        if DefaultPool.SafelyExit() == true {
            t.Error("SafelyExit not working right, cant turnoff pool when pool have one or more job")
        }
        DefaultPool.Done()
    })

    t.Run("turnoff pool when pool dont have any job", func(t *testing.T) {
        DefaultPool.Insert(taskName)
        DefaultPool.Done()
        if DefaultPool.SafelyExit() == false {
            t.Error("SafelyExit not working right, pool can be turned off when no job is in pool")
        }
    })
}