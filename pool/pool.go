package pool

var (
    PoolMaxSize=10
)

var (
    DefaultPool = &Pool{
        Jobs: make(chan string, PoolMaxSize),
        Size: 0,
    }
)

// if task is pause, it's jobs will not be excuted, but it still exists in pool
// if task is stop, all of it's jobs in pool wil remove
type Pool struct {
    Jobs chan string
    Size int // only can shutdown when size === 0
}

func (pool *Pool) Insert(taskName string) {
    if (pool.Size+1) > PoolMaxSize {
        return
    }
    pool.Size += 1
    go  func() {
        pool.Jobs <- taskName
    }()
}

func (pool *Pool) Done() {
    if pool.Size == 0 {
        return
    }
    pool.Size = pool.Size - 1
}

func (pool *Pool) SafelyExit() bool {
    return pool.Size  <= 0
}