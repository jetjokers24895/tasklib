package job

import (
    "testing"
)

func TestJob_CanRunSmooth(t *testing.T) {
    t.Run("job A", func(t *testing.T) {
        err := JobA()
        if err != nil {
            t.Error("Job A cant right working")
        }
    })
    t.Run("job B", func(t *testing.T) {
        err := JobB()
        if err != nil {
            t.Error("Job A cant right working")
        }
    })
    t.Run("job C", func(t *testing.T) {
        err := JobC()
        if err != nil {
            t.Error("Job A cant right working")
        }
    })
    t.Run("job D", func(t *testing.T) {
        err := JobD()
        if err != nil {
            t.Error("Job A cant right working")
        }
    })
}