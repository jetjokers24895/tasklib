package job

import (
	"fmt"
	"taskLib/params"
	_errors "taskLib/errors"
	"time"
)

func JobA() error {
    fmt.Println("iam in task A")
    return nil
}

func JobB() error {
    fmt.Println("iam in task B")
    return nil
}

func JobC() error {
    params.Set("from_c", time.Now().Unix())
    fmt.Println("iam in task C")
    return nil
}

func JobD() error {
    rfromC := params.Get("from_c")
    fromC, ok := rfromC.(int64)
    if !ok {
        panic(_errors.ErrParamsNotValid)
    }
    fmt.Println("iam in task D, args from C:", fromC)
    return nil
}