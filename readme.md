# TaskLib
## How To Run
you can use go get or clone this repository; then put it on your [gopath]
```
cd [gopath]/taskLib
go run main.go
```
## Test
Unit test in a dependencies folder`
you can run with
```
cd [gopath]/taskLib/manager
go test
```

or you can run test all with
```
cd  [gopath]/taskLib
go test -v ./...
```

## Examples
Examples in folder `./examples`
Have 4 examples for 4 case
```
- Task A -  run every 5 seconds
- Task B -  run At hh:mm:dd hour daily(time in utc)
- TASK C -  run every 5 seconds, and set params`
- TASK D -  run every 6 seconds, and get params from TaskC
```

## Explain
This repo have 4 main component
```
- Task Manager : ./manager: save task, and run all action to task
- Jobs Pool : ./pool
- Worker : ./worker - it subcribe jobs pool and do job
- TimeTicker with 1 second duration: with each tick, it will be check all task, if any task is running(state=running), and it's time at nextRun  < now, it will be push to job pool. And Worker will do that job.
```
### Graceful Shutdown
When typing `go run main.go`
TaskManager and Worker, JobPool, TimeTicker  will be created
when typing "Ctrl +C" or os.kill from anywhere
```
Step1. Shut down time ticker => no job will be pushed to pool
Step2. Every 5 Second, check have any job in the pool, if no job in the pool then exit the loop and otherwise continue the loop
Step3. Exit the rest
```
## Refer
https://github.com/hsxhr-10/scheduler
https://github.com/jasonlvhit/gocron