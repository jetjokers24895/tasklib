package manager

import (
	"fmt"
    "testing"
    "taskLib/scheduler"
    "reflect"

)

var (
    TypePointer = "ptr"
)

func sampleJobHandler() {
    fmt.Println("iam testing task!")
}

func TestManager(t *testing.T) {
    t.Run("init a manager", func(t *testing.T) {
        manager := NewManager()
        if manager.Storage == nil {
            t.Error("Storage of map was not initialized")
        }
    })
    taskName := "testingTask"
    t.Run("add a task", func(t *testing.T) {
        manager := NewManager()
        manager.Add(taskName, sampleJobHandler, scheduler.NewEvery(3,0,0))
        if len(manager.Storage) != 1 {
            t.Error("Add a task is not working right. After adding, len of storage should be 1")
        }

        typeOfTask := reflect.TypeOf(manager.Storage[taskName])
        if typeOfTask.Kind().String() != TypePointer {
            t.Error("Task saved in Manager Storage, it must be a pointer")
        }
    })

    t.Run("remove a task", func(t *testing.T) {
        manager := NewManager()
        manager.Add(taskName, sampleJobHandler, scheduler.NewEvery(3,0,0))
        manager.Remove(taskName)
        if len(manager.Storage) != 0 {
            t.Error("Remove a task is not working right. After removing, len of storage should be 0")
        }
    })
}
