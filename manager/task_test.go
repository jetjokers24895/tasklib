package manager

import (
	"fmt"
    "testing"
    "taskLib/scheduler"
    "time"
    "taskLib/pool"
)

func TestTask(t *testing.T) {
    const taskname = "testingTask"

    t.Run("start a task", func(t *testing.T) {
        task := Task{
        Name: taskname,
        Job : func() {fmt.Println("iam testing task!")},
        State: Pending,
        Period: scheduler.NewEvery(3,0,0),
        }
        task.Start()
        now := time.Now().UTC()
        if task.State != Running {
            t.Error("When start a Task, state of task must be 'Running'")
        }
        if task.NextTime.Unix() - now.Unix() != 3 {
            t.Error("Next time for doing job not right!")
        }
    })

    t.Run("test ShouldBeDoJob func", func(t *testing.T) {
        task := Task{
            Name: taskname,
            Job : func() {fmt.Println("iam testing task!")},
            State: Pending,
            Period: scheduler.NewEvery(3,0,0),
        }

        task.NextTime = task.Period.NextRun()
        time.Sleep(3 * time.Second)
        if task.ShouldBeDoJob() != true {
            t.Error("ShouldBeDoJob not working right")
        }
    })

    t.Run("test PushToPool func", func(t *testing.T) {
        task := Task{
            Name: taskname,
            Job : func() {fmt.Println("iam testing task!")},
            State: Pending,
            Period: scheduler.NewEvery(3,0,0),
        }

        task.PushToPool()
        now := time.Now().UTC()
        if task.NextTime.Before(now) {
            t.Error("func PushToPool must caculate next time for run")
            t.FailNow()
        }

        if task.NextTime.Unix() - now.Unix() != 3 {
            t.Error("Next time for doing job not right!")
        }
        if  pool.DefaultPool.Size != 1 {
            t.Error("PushToPool not working right, size of pool must be 1")
        }
    })
}