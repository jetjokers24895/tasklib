package manager

import (
	"reflect"
	"taskLib/pool"
	"taskLib/scheduler"
	"time"
)

var (
    Pending=0
    Running=1
    Pause=2
    Stop=3
)

type Task struct {
    Name string
    Job interface{}
    State int
    Period scheduler.Period
    LastTime time.Time
    NextTime time.Time

    Params interface{}
}

func(t *Task) DoJob() {
    j := reflect.ValueOf(t.Job)
    j.Call([]reflect.Value{})
    //TODO: save last time and calculate next Time
    t.LastTime = t.NextTime
    pool.DefaultPool.Done()
}

func (t *Task) ShouldBeDoJob() bool {
    now := time.Now().UTC()
    return now.After(t.NextTime) || now.Equal(t.NextTime)
}

func (t *Task) PushToPool() {
    pool.DefaultPool.Insert(t.Name)
    t.NextTime = t.Period.NextRun()
}

func(t *Task) Pause() {
    t.State = Pause
}

func(t *Task) Start() {
    t.State = Running
    t.NextTime = t.Period.NextRun()
}