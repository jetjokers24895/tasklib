package manager

import (
	"reflect"
	_errors "taskLib/errors"
	"taskLib/pool"
    "taskLib/scheduler"
)

type Manager struct {
    Storage map[string]*Task
}

func NewManager() *Manager {
    return &Manager{
        Storage: map[string]*Task{},
    }
}

//Add "add a task: {name}, {jobHandler}, interval"
func (m *Manager) Add(taskName string, jobHandler interface{}, period scheduler.Period) error {
    if m.CheckTaskExists(taskName) {
        return _errors.ErrTaskWasExists
    }

    if reflect.TypeOf(jobHandler).Kind() != reflect.Func {
        return _errors.ErrJobHandleMustBeFunction
    }
    task := &Task{
        Name: taskName,
        Job: jobHandler,
        State: Pending,
        Period: period,
    }
    m.Storage[taskName] = task
    return nil
}

func (m *Manager) Start(taskName string) {
    m.Storage[taskName].Start()
}

func (m *Manager) Stop(taskName string) {
    m.Storage[taskName].State = Pending
}

func (m *Manager) Pause(taskName string) {
    m.Storage[taskName].Pause()
}

func (m *Manager) Remove(taskName string) {
    // it's job are on pool will be not excuted
    delete(m.Storage, taskName)
}

func (m *Manager) Export() {}
func (m *Manager) Import() {}

func (m *Manager) CheckTaskExists(taskName string) bool {
    _, ok := m.Storage[taskName]
    return ok
}

func (m *Manager) IsSafelyExit() bool {
    return pool.DefaultPool.SafelyExit()
}

func (m *Manager) JobInPool() int {
    return pool.DefaultPool.Size
}